import logo from './logo.svg';
import './App.css';


import React, { Component } from 'react'

export default class App extends Component {
  constructor(){
    super();

    this.state={
      skills: [
        {
          id: 92389,
          name:"Reactjs",
        },
        {
          id: 5849,
          name:"Vue",
        },
        {
          id: 2343,
          name:"JavaScript",
        },
        {
          id: 5453,
          name:"jQuery",
        },
        {
          id: 32423,
          name:"Redux",
        },
        {
          id: 4554,
          name:"React Native",
        },
        {
          id: 2349,
          name:"React Developer",
        },
      ],
      keyword: "",
      query: "",
      filteredList: [],
      isTrue: false,
    }

 
    const List = (
      <div className="App">
        <textarea placeholder="Share your thoughts" onChange={(e) => this.spaceArea(e)}></textarea>
        {this.state.filteredList}
      </div>
    )

  render() {
    return (
      <div>
        <List />
      </div>
    );
  }
}
}

ReactDOM.render(
  <List />,
  document.getElementById('root')
);